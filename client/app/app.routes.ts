import {HomePageComponent} from './components/homepage/hompage.component';
import {AboutComponent} from './components/about/about.component';
import {ContactComponent} from './components/contact/contact.component';
import {SigninComponent} from './components/signin/signin.component';
import {SignupComponent} from './components/signup/signup.component';


export const BlogSiteApp = [
    { path: '', component: HomePageComponent },
    { path: 'homepage', component: HomePageComponent},
    { path: 'about', component: AboutComponent},
    { path: 'contact', component: ContactComponent},
    { path: 'signin', component: SigninComponent},
    { path: 'signup', component: SignupComponent}
];
