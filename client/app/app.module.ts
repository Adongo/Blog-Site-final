//Defines AppModule, the root module that tells Angular how to assemble the application.
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import {AppComponent}  from './app.component';
import {HomePageComponent} from './components/homepage/hompage.component';
import {AboutComponent} from './components/about/about.component';
import {ContactComponent} from './components/contact/contact.component';
import {SigninComponent} from './components/signin/signin.component';
import {SignupComponent} from './components/signup/signup.component';

import { BlogSiteApp } from './app.routes';

@NgModule({
  imports:      [
                  BrowserModule,
                  RouterModule.forRoot( BlogSiteApp)
                ],

  declarations: [
                  AppComponent ,
                  HomePageComponent,
                  AboutComponent,
                  ContactComponent,
                  SigninComponent,
                  SignupComponent
                ],

  bootstrap:    [ AppComponent ]
})
export class AppModule { }

