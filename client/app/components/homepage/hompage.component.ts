///<reference path="../../../node_modules/@types/node/index.d.ts"/>

import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'my-homepage',
    templateUrl: 'homepage.component.html'
})
export class HomePageComponent  {}
