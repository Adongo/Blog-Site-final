"use strict";
var hompage_component_1 = require('./components/homepage/hompage.component');
var about_component_1 = require('./components/about/about.component');
var contact_component_1 = require('./components/contact/contact.component');
var signin_component_1 = require('./components/signin/signin.component');
var signup_component_1 = require('./components/signup/signup.component');
exports.BlogSiteApp = [
    { path: '', component: hompage_component_1.HomePageComponent },
    { path: 'homepage', component: hompage_component_1.HomePageComponent },
    { path: 'about', component: about_component_1.AboutComponent },
    { path: 'contact', component: contact_component_1.ContactComponent },
    { path: 'signin', component: signin_component_1.SigninComponent },
    { path: 'signup', component: signup_component_1.SignupComponent }
];
//# sourceMappingURL=app.routes.js.map