var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');


var index = require('./routes/index');
var users = require('./routes/users');
var homepage = require('./routes/homepage');
var contact = require('./routes/contact');
var about = require('./routes/about');
var signin = require('./routes/signin');
var signup = require('./routes/signup');
var terms_conditions = require('./routes/terms_conditions');

var port = 3000;

var app = express();

//set view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//set static folder
app.use(express.static(path.join(__dirname, 'client')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


app.use('/', index);
app.use('/users', users);
app.use('/homepage', homepage);
app.use('/contact', contact);
app.use('/about', about);
app.use('/signin', signin);
app.use('/signup', signup);
app.use('/terms_conditions', terms_conditions);

app.listen(port, function () {
    console.log("server started on port "+port);
});

module.exports = app;
